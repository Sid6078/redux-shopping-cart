import { configureStore, combineReducers } from "@reduxjs/toolkit";

const reducerFn = combineReducers({
  // Add Reducers
});

const initialState = { counter: 0 };

const store = configureStore({
  reducerFn,
  preloadedState: initialState,
  devTools: true,
});

export default store;
